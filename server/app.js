const express = require('express');
const bodyParser = require('body-parser');
const records = require('./routes/records');
const login = require('./routes/login');
const mongoose = require('mongoose');

const app = express();
mongoose.connect('mongodb://localhost/sport');

app.use('/api/login', login);
app.use('/api/records', records);
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

mongoose.connection.on('error', (error) => console.error('Unable to connect to MongoDB instance.', error));
mongoose.connection.on('open', () => {
    console.log('MongoDB connected');
    app.listen(3000, () => console.log('Server started on port 3000'));
});