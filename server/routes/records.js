const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser')
const RecordService = require('../service/record.service');

router.use((req, res, next) => {
    console.log('Time: ', new Date());
    next();
});

router.get('/', (req, res) => {
    RecordService.getAllRecords((err, record) => {
        if(err){
            throw err;
        };
        res.json(record);
    });
});

router.get('/:_id', (req, res) => {
    let _id = req.params._id;
    RecordService.getRecordById(_id, (err, record) => {
        if(err){
            throw err;
        };
        res.json(record);
    });
});

router.post('/', bodyParser.json(), (req, res) => {
    let name = req.body.name;
    let type = req.body.type;
    let duration = req.body.duration;
    RecordService.addRecord(name, type, duration);
    console.log("record added .. ")
});


module.exports = router;