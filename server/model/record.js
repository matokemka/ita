const mongoose = require('mongoose');

let recordSchema = mongoose.Schema({
    name: {
        type: String
    },
    type: {
        type: String
    },
    duration: {
        type: String
    },
    created: {
        type: String
    }
})

let RecordModel = module.exports = mongoose.model('Record', recordSchema, 'record');