const express = require('express');
const router = express.Router();
const RecordRepository = require('../record_repository/repository')


class RecordService {
    constructor() {

    }

    getAllRecords(callback) {
        RecordRepository.getAllRecords(callback);
    }

    getRecordById(_id, callback) {
        RecordRepository.getRecordById(_id, callback);
    }

    addRecord(name, type, duration) {
        RecordRepository.addRecord(name, type, duration);
    }
}

module.exports = new RecordService();