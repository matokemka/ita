const express = require('express');
const router = express.Router();
const RecordModel = require('../model/record')
const Record = require('../model/record');


class RecordRepository {

    addRecord(name, type, duration){
        let recordModel = new RecordModel({name, type, duration, created : Date.now()});
        return recordModel.save();
    }

    getAllRecords(callback){
        console.log('Getting records now ...');
        Record.find(callback);
    }

    getRecordById(_id, callback){
        console.log('Getting the record now ...');
        Record.findById(_id, callback);
    }

}

module.exports = new RecordRepository();