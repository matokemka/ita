const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const commonConfig = require('./webpack.common.js');
const helpers = require('./helpers');
const path = require('path');

module.exports = webpackMerge(commonConfig, {
    devtool: 'source-map',

    output: {
        path: helpers.root('dist'),
        publicPath: 'http://127.0.0.1:4000/',
        filename: '[name].js',
        chunkFilename: '[id].chunk.js'
    },

    plugins: [
        new ExtractTextPlugin({
            filename: '[name].css'
        }),
        new HtmlWebpackPlugin({
            template: 'src/index.ejs',
            baseUrl: '/'
        })
    ],

    devServer: {
        historyApiFallback: true,
        stats: 'minimal',
        port: 4000,
        proxy: {
            '/api/**': {
                target: 'http://localhost:3000/'
            }
        }
    }
});
