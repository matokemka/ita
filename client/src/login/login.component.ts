import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {Http} from '@angular/http';
import {LoginService} from '../service/login.service'

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    providers: []
})

export class Login implements OnInit {

    constructor(private router : Router, private loginService : LoginService) {
    }

    ngOnInit() {

    } 

    onClick(){
        console.log('starting onClick');
        this.loginService.login().subscribe(token => {
        localStorage.setItem('user7', JSON.stringify({token}));
        console.log('onClick done');
        this.router.navigate(['/records/list']);
        })  
    }

}