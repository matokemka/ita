import {NgModule, OnInit, Directive} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {RecordComponent} from "./record.component";
import {RecordListComponent} from "./record.list.component";
import {RecordAddComponent} from './record.add.component';
import {FormsModule} from "@angular/forms";
import {HttpModule, JsonpModule } from "@angular/http";
import {RecordService} from "../service/record.service";
import { RouterModule } from '@angular/router';
import { DurationPipe } from "../pipe/duration.pipe";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        JsonpModule,
        RouterModule,
        FormsModule
    ],
    declarations: [
        RecordComponent,
        RecordListComponent,
        RecordAddComponent,
        DurationPipe
    ],
    providers: [DurationPipe],
    exports: [
        RecordComponent,
        RecordListComponent,
        RecordAddComponent,
        DurationPipe
    ],
})

export class RecordModule {
}