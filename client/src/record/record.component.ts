import {Component, Input, OnInit, ViewEncapsulation} from "@angular/core";
import {Record} from "../model/record";
import {RecordService} from "../service/record.service";
import {RecordListComponent} from './record.list.component';
import {RecordAddComponent} from './record.add.component';

@Component({
    selector: 'rc',
    templateUrl: './record.component.html' ,
})

export class RecordComponent implements OnInit{

private record : Record[];

constructor(private recordsService : RecordService) {

}

ngOnInit() {

}

}