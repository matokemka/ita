import {Component, Input, OnInit, ViewEncapsulation} from "@angular/core";
import {Record} from "../model/record";
import {RecordService} from "../service/record.service";
import {NgForm} from '@angular/forms';

@Component({
    selector: 'rac',
    templateUrl: './record.add.component.html',
    providers: []
})

export class RecordAddComponent implements OnInit{

private types = [
    'beh',     
    'plavanie',    
    'bicykel',
    'beh vo vreci'
];

private record = new Record();


constructor(private recordsService : RecordService) {

}

ngOnInit() {
}

upload(){
    this.recordsService.addRecord(this.record).subscribe();
    console.log('uploaded');
}

}