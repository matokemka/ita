import {Component, Input, OnInit, ViewEncapsulation} from "@angular/core";
import {Record} from "../model/record";
import { RecordService } from "../service/record.service";
import { DurationPipe } from "../pipe/duration.pipe";

@Component({
    selector: 'rlc',
    templateUrl: './record.list.component.html',
    providers: [DurationPipe],
})

export class RecordListComponent implements OnInit{

private record : Record[];

constructor(private recordsService : RecordService) {
}

ngOnInit() {
    this.recordsService.getAllRecords().subscribe(records => {
        this.record = records; 
       });
}

}
