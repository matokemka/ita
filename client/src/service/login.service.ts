import {Injectable, Input} from '@angular/core';
import {Record} from '../model/record';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import HttpService from './http.service';

@Injectable()
export class LoginService {

    constructor(private httpService : HttpService){

    }

    login() {
        console.log('login in LoginService done');
        return this.httpService.get('/api/login');
    }
    
    
}