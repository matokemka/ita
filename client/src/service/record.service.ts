import {Injectable, Input} from '@angular/core';
import {Record} from '../model/record';
import {Http, RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import HttpService from './http.service';

@Injectable()

export class RecordService {

    constructor(private httpService : HttpService){
    }

    getAllRecords() {
        return this.httpService.get("/api/records/");
    }
    
    addRecord(record:Record) {
        console.log('adding record');
        return this.httpService.post("api/records/", record);
    }
    
}