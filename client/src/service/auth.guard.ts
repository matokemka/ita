import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthHttp, AuthConfig, tokenNotExpired } from 'angular2-jwt';
//import { jwt } from 'jwthelper'

@Injectable()

export class AuthGuard implements CanActivate {

    constructor(private router: Router) {}



    canActivate() {
        if (tokenNotExpired(null,localStorage.getItem('user7'))) {
            console.log('activated in AuthGuard');
            console.log(tokenNotExpired(null,localStorage.getItem('user7')))
            return true;
        }

        this.router.navigate(['/login']);
        console.log('not activated in AuthGuard');
        return false;
    }
}