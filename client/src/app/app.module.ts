import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {AppComponent} from "./app.component";
import {FormsModule} from "@angular/forms";
import {HttpModule, JsonpModule} from "@angular/http";
import {AUTH_PROVIDERS} from 'angular2-jwt'
import {RouterModule, Routes} from '@angular/router'
import {RecordModule} from "../record/record.module";
import {AppRoutingModule} from '../router/router.module';
import {LoginModule} from '../login/login.module';
import HttpService from '../service/http.service';
import { RecordService } from "../service/record.service";
import { LoginService } from "../service/login.service";

const appRoutes : Routes = [];

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        JsonpModule,
        RecordModule,
        AppRoutingModule,
        RouterModule.forRoot(appRoutes),
        LoginModule
    ],
    declarations: [
        AppComponent,
    ],
    providers: [
        AUTH_PROVIDERS,
        HttpService,
        RecordService,
        LoginService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}