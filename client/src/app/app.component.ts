import {Component, ViewEncapsulation} from "@angular/core";

@Component({
    selector: 'my-app',
    templateUrl: './app.component.html',
    styleUrls: ['../../public/styles/styles.scss'],
    encapsulation: ViewEncapsulation.None
})


export class AppComponent {

}
