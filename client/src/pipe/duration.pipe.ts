import { Component, Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'duration'})

export class DurationPipe implements PipeTransform {
  transform(s: number): string {
    let h : number = Math.floor(s/3600);
    s -= 3600*h;
    let m : number = Math.floor(s/60);
    s -= 60*m;
    function two(x){
        return x > 9 ? x : "0" + x;
    }
    return (two(h)+':'+two(m)+':'+two(s));
  }
}