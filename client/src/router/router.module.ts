import { NgModule } from '@angular/core';
import { Router, Routes, RouterModule } from '@angular/router';
import { RecordComponent } from '../record/record.component';
import { Login } from '../login/login.component';
import { RecordListComponent } from '../record/record.list.component';
import { RecordAddComponent } from '../record/record.add.component';
import { AuthGuard } from '../service/auth.guard';

const appRoutes : Routes = [
    { path : '', redirectTo: 'login', pathMatch: 'full' },
    { path : 'records' , component: RecordComponent, children: 
      [
          { path : '', redirectTo: 'list', pathMatch: 'full' },
          { path : 'list', component: RecordListComponent, canActivate: [AuthGuard] },
          { path : 'add', component: RecordAddComponent, canActivate: [AuthGuard] }
      ],
    },
    { path : 'login', component: Login},
    { path : '**', redirectTo: 'login', pathMatch: 'full' }
    ]

@NgModule({
    imports: [RouterModule.forChild(appRoutes)],
    exports: [RouterModule],
    declarations: [],
    providers: [AuthGuard],
})

export class AppRoutingModule {

}
